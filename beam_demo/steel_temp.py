def steel_temp(time, Temp, rho_a, S_F, alpha_c, emissivity):
    """
    Temperature of steel (David Lange 2016, translated from MATLAB to Numpy by Ola Widlund)

    function [theta_s] = steel_temp (time,Temp,rho_a,S_F, alpha_c,emissivity)
    """

    # intitialise variables

    n = time.size # L_dim

    theta_s = ones_like(time)*293
    c_a = ones_like(time)*(425+0.773*(20)-0.00169*(20)**2+0.00000222*(20)**3)

    for i in range(n):

        if theta_s[i-1]<(600+273):
            c_a[i] = 425+0.773*(theta_s[i-1]-273)-0.00169*(theta_s[i-1]-273)**2+0.00000222*(theta_s[i-1]-273)**3
        elif theta_s[i-1]<(735+273):
            c_a[i] = 666+13002/(738.-(theta_s[i-1]-273))
        else:
            c_a[i] = 650.

        h_net = 5.678e-14*emissivity*((Temp[i])**4 - (theta_s[i-1])**4) + alpha_c*(Temp[i] - theta_s[i-1])

        Delta_Ts = S_F * h_net*(time[i]-time[i-1])/(c_a[i]*rho_a)

        theta_s[i] = theta_s[i-1] + Delta_Ts

    return theta_s
