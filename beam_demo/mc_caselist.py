### Script to prepare an abaqus input file for studying a steel beam under
### 4 point bending using parameters to define the geometry, material
### properties and the loading. Beam is exposed to fire, but the
### uncertainty wrt this is handled outside of abaqus in the script.
###
### David Lange, Johan Anderson
###
#  Uncertain parameters are:
# beam_length
# flange_width
# flange_thickness
# web_depth
# web_thickness
# load_posn1
# load_posn2
# c_s
# rho_s
# E_s 
# sigma_y_sa 
# density 
# elongation 
# Load

from numpy.random import RandomState
import numpy as np
import parsim.core

# Values of parameters with fixed values

fixed_parameters = \
    {
        'beam_length': 5200.0,
        'flange_width': 300,
        'flange_thickness': 19.,
        'web_depth': 300-2*19,
        'web_thickness': 11.,
        'load_posn1': 1400.,
        'load_posn2': 3800.,
        'c_s': 460.,
        'density': 7850e-9
    }

# Number of Monte Carlo samples

N = 100

# Initialize random number generator, with some seed value

rnd = RandomState(1234)

# Sampled parameters (arrays)

varying_parameters = \
    {
        'job_id': range(1, N+1),
        'E_s': rnd.normal(210000., 4000, N),         # Youngs Modulus of steel
        'sigma_y_sa': rnd.normal(355, 7, N),         # Yield stress of steel
        'elongation': rnd.normal(14e-6, 0.3e-6, N),  # Thermal expansion, gradient
        'Load': rnd.normal(-100000., 2000, N),
        'h_c': rnd.normal(25e-6, 5.7e-6, N),
        'emissivity': rnd.normal(0.7, 0.115, N)
    }

case_names = ["Job%s" % i for i in varying_parameters['job_id']]

parsim.core.create_caselist_file('mc.caselist', case_names, column_dict=varying_parameters)
parsim.core.create_parameter_file('mc.parameters', fixed_parameters)
