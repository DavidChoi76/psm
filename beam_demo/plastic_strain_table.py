import numpy as np
import math

def plastic_strain_table(E_s, sigma_y_sa):
    """
    Prepare plastic strain tables in abaqus format ((David Lange 2016, translated from MATLAB to Numpy by Ola Widlund)

    function [plastic_stress, plastic_strain] = plastic_strain_table(E_s,sigma_y_sa);
    """

    # T = [293, 373, 473, 573, 673, 773, 873, 973, 1073, 1173, 1273, 1373]

    E_st = E_s * np.array([1, 1, 0.9, 0.8, 0.7, 0.6, 0.31, 0.13, 0.09, 0.0675, 0.045, 0.0225])

    Sigma_p = sigma_y_sa * np.array([1, 1, 0.807, 0.613, 0.42, 0.36, 0.18, 0.075, 0.05, 0.0375, 0.025, 0.0125])

    Sigma_y = sigma_y_sa * np.array([1, 1, 1, 1, 1, 0.78, 0.47, 0.23, 0.11, 0.06, 0.04, 0.02])

    eps_y = 0.02

    abs_strain = np.zeros(18)
    stress = np.zeros(18)
    irreversible_strain = np.zeros(18)

    plastic_stress = np.zeros((12, 18))
    plastic_strain = np.zeros((12, 18))

    for i in range(12):
        for j in range(18):
            eps_p = Sigma_p[i]/E_st[i]
            if j==1:
                abs_strain[j] = eps_p
            else:
                abs_strain[j] = abs_strain[j-1]+eps_p

            c = (Sigma_y[i]-Sigma_p[i])**2/((eps_y - eps_p)*E_st[i]-2*(Sigma_y[i]-Sigma_p[i]))
            b = math.sqrt(c*(eps_y - eps_p)*E_st[i]+c**2)
            a = math.sqrt((eps_y - eps_p)*(eps_y - eps_p+c/E_st[i]))

            if abs_strain[j] == eps_p:   # Equality of floats?
                stress[j] = Sigma_p[i]
            elif abs_strain[j]<eps_y:
                stress[j] = Sigma_p[i]-c+(b/a)*math.sqrt(a**2-(eps_y-abs_strain[j])**2)
            else:
                stress[j] = Sigma_p[i]

            # irreversible_strain[j] = abs_strain[j]-stress[j]/E_st[i]

        irreversible_strain = abs_strain - stress/E_st[i]

        plastic_stress[i, :] = stress
        plastic_strain[i, :] = irreversible_strain

    return (plastic_stress, plastic_strain)









        
            