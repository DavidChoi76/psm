Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_,
and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.


[2.1.0] - 2020-08-03
--------------------

Added
.....

* Special parameters with parsim-related case information are available for macro expansion on case creation.
  They are also available as environent variables of the subprocesses started with the ``psm run`` command.


[2.0.0] - 2019-09-27
--------------------

Added
.....

* Implemented DOE scheme `gsd` (Generalized Subset Design), as available in pyDOE2 package.
  Allows reduced factorial designs with more than two levels.
* With DOE scheme `fracfact`, the user can now define the reduced design either by a generator
  expression (option "gen"), or by the design resolution (option "res").
* Property-based getter functions in `Case` and `Study` classes now provide caselist, results and
  parameter info as pandas `DataFrame` or `Series` objects.
* `Study.collect()` method now aggregates all collected results in a file "study.results" in the
  study directory. This file includes also cases with missing data (marked as "NaN", pandas-style).
* `pyDOE2`, `numpy`, `scipy` and pandas are now mandatory dependencies.
* Constructor of `Case` class now handles colon-separated format <study>:<case> for cases inside studies.
  This simplifies use of the parsim Python API for working with results.
* Examples of how to use the Python API for post-processing and data analysis.

Removed
.......

* Support for Python 2 has been removed!
* Config option `paramlist_upper_names` has been removed (controlled optional automatic conversion
  of all parameter names to uppercase).

Changed
.......

* API changes in ParsimObjects (incl. subclasses `Project`, `Study` and `Case`), especially in
  constructors (`__init__`) and `create()` method. These changes makes the CLI command implementations
  shorter and easier to understand. Better checking of sane `name` and `path` arguments to constructor.


[1.0.0] - 2018-12-19
--------------------

Changed
.......

* Use `pyDOE2 package <https://github.com/clicumu/pyDOE2>`_, instead of pyDOE.

Fixed
.....

* Now works with Python 3 (and Python 2.7, as before).
* Fixed problems with parsing of DOE command-line arguments.

[0.7.0] - 2018-07-26
--------------------
