To Do
-----

The following functionality is currently planned or under development:

* More work on Sphinx documentation.
    Especially tutorial section. Also documentation of Python API (ParsimObjects).

* Subcommand ``psm copy``:
    Copy of a single file or subdirectory to an individual case, or to all cases in a study.

* Subcommand ``psm arch``:
    Selective archiving of cases and studies, as described by include and exclude file name patterns.

* Functionality for adding/merging cases to existing studies.
    Should create caselist file and parameter file for recreating all cases of a study.

* Sub-selection of cases in a study.
    All commands operating on studies, for example copying, archiving and running of scripts.

* Implementation of functionality for sampling and DOE (Design Of Experiments).
