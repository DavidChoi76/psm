.. parsim documentation master file, created by
   sphinx-quickstart on Tue Apr 26 15:21:40 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _contents:

Parsim documentation contents
=============================

.. toctree::
   :maxdepth: 2

   intro
   basics
   tutorial
   parameterization
   doe
   dakota
   cli
   api
   installation
   glossary
   using_API
   changes

..
   parameters
   projects
   reference
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

