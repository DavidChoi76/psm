mc,Monte Carlo random sampling.
ff2n,Two-level full factorial sampling.
fullfact,General full factorial sampling (for more than two levels).
fracfact,Two-level fractional factorial sampling.
ccdesign,Central Composite Design (CCD).
lhs,Latin Hypercube sampling.
pb,Plackett-Burman (pbdesign).
gsd,Generalized Subset Design (GSD)
